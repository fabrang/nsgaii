from __future__ import absolute_import

import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nsga",
    version="0.0.1",
    install_requires=['numpy', 'pandas'],
    author="Fabian Rang",
    author_email="fabian.rang@yahoo.com",
    description="Package for genetic algorithm NSGA-II",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)