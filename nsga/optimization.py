import pandas as pd
from .utils import *
from .objective import BestSurrogats
from copy import deepcopy as dp
import logging


class Evolution(object):
    """
    With an initial population the NSGA-II is performed here and searches optimizied solutions for the defined
    kpis.
    """

    def __init__(self, people, models, meta, max_gen=100, size=400):

        self.size = size
        self.max_gen = max_gen
        self.meta = meta
        self.constraints = meta['constraints']  # See how it has to be integrated!
        self.population = create_population(people)
        self.offspring = None
        self.combined_pop = dp(self.population)
        self.surrogats = BestSurrogats(models)
        self.result = pd.DataFrame()

    def evolve(self):
        # See where the evolve handler is placed
        for i in range(self.max_gen):
            self.offspring = produce_offspring(self.population,
                                               self.meta,
                                               self.surrogats)
            if not self.offspring:
                break
            self.offspring = constraint_score(self.offspring, self.constraints)
            for individual in self.offspring:
                self.combined_pop.population.append(individual)
            self.combined_pop = fast_non_dominant_sort(self.combined_pop, len(self.constraints) != 0)
            self.population = reset_pop(self.population)
            self.offspring = []

            for j in range(len(self.combined_pop.fronts)):
                if len(self.population.population) + len(self.combined_pop.fronts[j]) < self.size:
                    self.combined_pop.fronts[j] = crowding_distance(self.combined_pop.fronts[j])
                    for person in self.combined_pop.fronts[j]:
                        self.population.population.append(person)
                else:
                    self.combined_pop.fronts[j] = crowding_distance(self.combined_pop.fronts[j])
                    for individual in reversed(sort_front(self.combined_pop.fronts[j])):
                        if len(self.population.population) < self.size:
                            self.population.population.append(individual)
                        elif len(self.population.population) == self.size:
                            break
                    break

            #self.save_genes(i)
            self.combined_pop = reset_pop(self.combined_pop)
            self.combined_pop = dp(self.population)

        self.population = fast_non_dominant_sort(self.population, len(self.constraints) != 0)
        dna = [a.dna for a in self.population.fronts[0]]
        kpis = [a.kpis for a in self.population.fronts[0]]
        # todo: Needs to be integrate as a live plotting env. where we can permanenty see the progress of nsga2
        kpi1 = []
        kpi2 = []
        for kpi in kpis:
            kpi1.append(kpi[0])
            kpi2.append(kpi[1])

        self.result.reset_index(drop=True, inplace=True)

    # todo: Needs to be adapted if there are more than 2 inputs or outputs ...
    """
    def save_genes(self, generation): 
        dict = {'generation': generation,
                'param1': [individual.dna[0] for individual in self.combined_pop.population],
                'param2': [individual.dna[1] for individual in self.combined_pop.population],
                'target1': [individual.kpis[0] for individual in self.combined_pop.population],
                'target2': [individual.kpis[1] for individual in self.combined_pop.population],
                'rank': [individual.rank for individual in self.combined_pop.population],
                'distance': [individual.distance for individual in self.combined_pop.population]}


        tmp = pd.DataFrame(dict)
        self.result = pd.concat([self.result, tmp])#
        """
