from .struct.population import Person, Population
import random
import numpy as np

# Todo: We need to eliminate all points that have the same design! Otherwise we divide through 0

def constraint_score(population, constraints):
    """
    In this function all individuals of a score are penalised if they don't satisfy the given constraints
    Make one function, that takes all the input params and gives back a score for every constraint
    which is not fulfilled 1 is added to the score
    :param population: List of individuals in a population
    :param constraints: List of all defined constraints
    :return: population with weighted constraint scores
    """
    for individual in population:
        individual.violation = 0
        for constraint in constraints:
            if not constraint(individual.dna):
                individual.violation += 1

    return population


def crowding_distance(front):
    """
    :param front: the frontier for which the crowding distance for each point need to be calculated
    :return: frontier with updated crowding distance value
    """
    valued_front = front
    length = len(valued_front[0].kpis)
    size = len(valued_front)
    for j in range(size):
        valued_front[j].distance = 0
    for m in range(length):
        valued_front = sorting(valued_front, m)
        valued_front[0].distance = 99999999999
        valued_front[size - 1].distance = 99999999999
        for i in range(1, size - 1):
            # Todo: We need to see here how we can handle and kick out when frontier consists of more often the same val
            if valued_front[size - 1].kpis[m] == valued_front[0].kpis[m]:
                pass
            # Todo: Matlab runtimeWarning: invalid value encountered in double_scalars > Perhaps Division by Zero
            valued_front[i].distance = valued_front[i].distance + \
                                       (valued_front[i + 1].kpis[m] - valued_front[i - 1].kpis[m]) / \
                                       (valued_front[size - 1].kpis[m] - valued_front[0].kpis[m])

    return valued_front


def sorting(front, objective):
    """
    :param front: frontier to which the sorting needs to be applied
    :param objective: index of the objective value
    :return: sorted frontier based on the defined objective it is sorted for
    """
    sorted_front = []
    for individual in front:
        if len(sorted_front) == 0:
            sorted_front.append(individual)
        else:
            for i in range(len(sorted_front)):
                if individual.kpis[objective] < sorted_front[i].kpis[objective]:
                    sorted_front.insert(i, individual)
                    break
                elif i == (len(sorted_front) - 1):
                    sorted_front.append(individual)

    return sorted_front


def sort_front(front):
    """
    :param front: frontier where the crowding distances need to be sorted
    :return: sorted front based on the ranking of the crowding distance from the smallest
    first to the biggest last
    """
    # Todo: Make this more efficient
    sorted_front = []
    for individual in front:
        if len(sorted_front) == 0:
            sorted_front.append(individual)
        else:
            for i in range(len(sorted_front)):
                if individual.distance < sorted_front[i].distance:
                    sorted_front.insert(i, individual)
                    break
                elif i == (len(sorted_front) - 1):
                    sorted_front.append(individual)
    a = [a.distance for a in sorted_front]
    return sorted_front


def fast_non_dominant_sort(pop, constraints=False):
    """
    Finds all non-dominant population members in a given population
    :type pop: Population() Class
    :param pop: Population
    :param constraints: list containing all constraints that need to be satisfied
    :return: population with sorted fronts based on their non-dominant performance
    """
    # Todo: Make this work in parrallel
    pop.fronts = []
    pop.fronts.append([])
    # todo: This needs to be more generic in order to perform better
    if not constraints:
        for person in pop.population:
            person.dominates = set()
            person.is_dominated = 0
            person.rank = 0
            # Todo: When there is two times exactly the same point both are saved, this needs to change!!!
            for other_person in pop.population:
                if dominates(person, other_person):
                    person.dominates.add(other_person)
                elif dominates(other_person, person):
                    person.is_dominated += 1
            if person.is_dominated == 0:
                pop.fronts[0].append(person)

    # Syntax needs to be changed definetively !!!
    else:
        for person in pop.population:
            person.dominates = set()
            person.is_dominated = 0
            person.rank = 0

            for other_person in pop.population:
                if constraint_dominates(person, other_person):
                    person.dominates.add(other_person)
                elif constraint_dominates(other_person, person):
                    person.is_dominated += 1
            if person.is_dominated == 0:
                pop.fronts[0].append(person)

    # Somewehre here a empty front is created always after all persons are already matched somewhere
    # TODO: Check out where this happens and eliminate it
    i = 0
    while len(pop.fronts[i]) > 0:
        next_front = []
        for person in pop.fronts[i]:
            for other_person in person.dominates:
                other_person.is_dominated -= 1
                if other_person.is_dominated == 0:
                    other_person.rank = i + 1
                    next_front.append(other_person)
        i += 1
        if len(next_front) > 0:
            pop.fronts.append(next_front)
        else:
            break

    return pop


def create_person(dna, kpis=None):
    """
    :param dna: Design parameters
    :param kpis: Target indicators given out from a simulation or a model
    :return: person class
    """
    person = Person(dna, kpis)
    return person


def create_population(people, constraints=[]):
    """
    :param people: list of lists containing the dna and the performance of peoples
    :param constraints: list of constraints that need to be satisfied
    :rtype: Population() Class
    :return: population
    """
    pop = Population()
    for i in range(len(people)):
        dna = people[i][0]
        kpis = people[i][1]
        pop.population.append(create_person(dna, kpis))
    if len(constraints) > 0:
        pop.population = constraint_score(pop.population, constraints)
    pop = fast_non_dominant_sort(pop)
    for front in pop.fronts:
        # TODO: Parse the list with constraint functions to constraint_score()
        front = crowding_distance(front)
    return pop


def reset_pop(pop):
    """
    :param pop: population to be reseted
    :return: resetted population
    """
    pop.population = []
    pop.fronts = []
    return pop


def dominates(a: np.array, b:np.array):
    """
    Checks if person a dominates individual b
    :param a: Person which is checked on dominance
    :param b: Person which is checked if it is dominanted
    :return: True or False depending if a dominates b or not


    if (a.kpis[0] <= b.kpis[0] and a.kpis[1] <= b.kpis[1]) and (a.kpis[0] < b.kpis[0] or a.kpis[1] < b.kpis[1]):
        return True
    return False
    """
    # Todo: Somewhere it seems that when two individuals are the SAME that both are taken into the pareto optimal front
    # Todo: Kill equivalent individuals
    if (a.kpis <= b.kpis).all() and (a.kpis < b.kpis).any():
        return True
    return False




def constraint_dominates(a, b):
    if a.violation < b.violation:
        return True
    elif a.violation == 0 and b.violation == 0:
        return dominates(a, b)
    else:
        return False


def crowding_operator(a, b):
    """
    Compares two individuals with each other
    :param a: Individual for which it is checked if it has a higher partitial order
    :param b: Individual to which the first one is compared
    :return: When a has a higher partial order than b it returns one, else zero
    """
    if (a.rank < b.rank) or (a.rank == b.rank and (a.distance > b.distance)):
        return 1
    return 0


def produce_offspring(population, borders, surrogats):
    """
    :param population: The population out of which the offspring is created
    :param borders: borders of the dna for single indivudals
    :param surrogats: A list/dict? with all chosen surrogat models for every param
    :return: A new population containing the parent and offspring population with the size 2*len(population)
    """
    # Todo IMPLEMENT the right functions for children
    # Todo Distinguish between continious / discrete / customized

    children = []
    while len(children) < len(population.population):
        parent1 = __tournament(population)
        parent2 = parent1
        i = 0
        # Todo We need to See that here it doesnt get in a endless loop !!!
        while np.array_equal(parent1.dna, parent2.dna):
            i += 1
            parent2 = __tournament(population)
            if i > 100:
                return False
        child1, child2 = __crossover(parent1, parent2)
        __mutate(child1, borders)
        __mutate(child2, borders)
        surrogats.calculate_objectives(child1)
        surrogats.calculate_objectives(child2)
        children.append(child1)
        if len(children) < len(population.population):
            children.append(child2)

    return children


def __crossover(individual1, individual2, crossover_size=1):
    """
    :param individual1: first parent individual
    :param individual2: second parent individual
    :param crossover_size: how many dna fragments are changed
    :return: two new children based on a crossover of individual1 and individual2
    """
    # TODO: implement make the function working independently first how do we generate an Individual?
    # TODO: Make sure the crossover can be adapted and varied
    child1 = create_person(individual1.dna)
    child2 = create_person(individual2.dna)
    # Child needs empty dna values
    genes_indexes = range(len(individual1.dna))
    half_genes_indexes = random.sample(genes_indexes, crossover_size)
    for i in genes_indexes:
        if i in half_genes_indexes:
            child1.dna[i] = individual2.dna[i]
            child2.dna[i] = individual1.dna[i]
        else:
            child1.dna[i] = individual1.dna[i]
            child2.dna[i] = individual2.dna[i]
    return child1, child2


def __mutate(child, meta, number_of_genes_to_mutate=1, rounded=2):
    """
    :param child: individual which has the chance to mutate one if its genes
    :param meta: meta for all design params including continious/discrete and min and max values
    :param number_of_genes_to_mutate: number of genes to be mutated
    :param rounded: after which decimal after comma numbers should be rounded
    :return: individual with mutated dna
    """
    # TOdo: check if we really need the rounded input ??? or can values stay raw
    # TODO: How do i do a good mutation without always resampling randomly a new value?
    genes_to_mutate = random.sample(range(0, len(child.dna)), number_of_genes_to_mutate)
    for gene in genes_to_mutate:
        if meta['vals'][gene][0] == 'continuous':
            mutation_power = meta['vals'][gene][1]
            child.dna[gene] = mutation_power[0] + round(random.random(), rounded) * (
                    mutation_power[1] - mutation_power[0])
        elif meta['vals'][gene][0] == 'discrete':
            mutation_power = meta['vals'][gene][1]
            child.dna[gene] = mutation_power[0] + round(round(random.random(), rounded) * (
                    mutation_power[1] - mutation_power[0]))
            raise NotImplementedError
        elif meta['vals'][gene][0] == 'custom':
            child.dna[gene] = np.random.choice(meta['vals'][gene][1], 1)[0]

    return child


def __tournament(population, n=2):
    """
    :param population: population from which the tournament participants are chosen from
    :param n: amount of participants are taken for a tournament selection
    :return: winner individual of the comparision of all tournament participants
    """
    #Todo: See if here there is a more generic way and what is the best number for n!
    participants = random.sample(population.population, n)
    best = None
    for participant in participants:
        if best is None or crowding_operator(participant, best) == 1:
            best = participant
    return best
