from nsga.optimization import Evolution
import pandas as pd
import numpy as np

class ProblemDefinitions():
    """
    Here all dynamic formulas are parsed and defined for the given problem.
    It is important, that for the biggest possible flexibility always the
    whole individual is parsed to the functions not only single parts like dna
    or the kpis.
    """
    def __init__(self):
        self.meta_info = None
        # With the functions we need to declare which function gives back which kpis
        self.functions = [f1, f2]
        self.constraints = [constraint1]
        raise NotImplementedError

    @staticmethod
    def f1(x1, x2):
        value = -x1 - x2**2 + x2
        return value

    @staticmethod
    def f2(self, x1, x2):
        value = -(x1-2)**2 + x2
        return value

    @staticmethod
    def constraint1(individual):
        if individual.dna[1] + individual.dna[0] <= 120:
            return True
        return False

    @staticmethod
    def constraint2(individual):
        raise NotImplementedError

    def perfect_pareto_front(self):
        raise NotImplementedError



if __name__ == '__main__':

    problem = ProblemDefinitions()

    param1 = np.random.randint(-40, 70, size=100)
    param2 = np.random.randint(-10, 200, size=100)
    size = len(param1)

    meta_df = pd.DataFrame({'param1': param1,
                            'param2': param2,
                            'target1': [function1(param1[i], param2[i]) for i in range(size)],
                            'target2': [function2(param1[i], param2[i]) for i in range(size)]})

    meta_info = {'dna': [{'name': 'param1', 'min': -40, 'max': 70},
                         {'name': 'param2', 'min': -10, 'max':200}],
                 'kpis': [{'name': 'target1', 'to': 'max'},
                          {'name': 'target2', 'to': 'max'}]}

    models = {'to_exec': problem.functions, 'type': 'function', 'constraints': problem.constraints}

    run_evolution = Evolution(100, meta_df, meta_info, 100, models)
