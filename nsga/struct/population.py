import numpy as np


class Population(object):
    """
    This class contains the population for the specific generation and the resulting pareto front
    """
    def __init__(self):
        """
        :param kpis: dict with all kpis and if they need to be maximized or minimized
        self.population: List of individuals which are part of the population
        self.front: List of non-dominant individuals in the population
        self.kpi_index: Dict which tells if the kpi needs to be maximized or minimized
        """
        self.population = []
        self.fronts = []


class Person(object):
    """
    This class is for generating individuals with the needed properties
    """
    def __init__(self, dna, kpis):
        """
        self.rank: The rank of the individual in the population
        self.dominates: The set of individuals that are dominated by this individual
        self.is_dominated: The amount of individuals that dominate this individual
        self.dna: Set of design variables
        self.kpis: Set of target variables
        """
        self.rank = None
        self.distance = None
        self.violation = 0
        self.dominates = set()
        self.is_dominated = 0
        self.dna = np.array(dna)
        self.kpis = np.array(kpis)
