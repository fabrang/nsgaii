"""
In this document the different functions are passed
"""
import numpy as np

class BestSurrogats(object):

    def __init__(self, models):
        self.models = models

    def calculate_objectives(self, individual):
        """
        :param individual:
        :return:
        """
        kpis = []
        for kpi in range(len(self.models)):
            kpis.append(self.models[kpi](individual.dna)) #[kpi])

        individual.kpis = np.array(kpis)