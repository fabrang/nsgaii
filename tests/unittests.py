import unittest
import numpy as np
from ..nsga import utils
from ..nsga import Person
from copy import deepcopy as dp


class TestUtils(unittest.TestCase):
    # Todo: Check the difference between normal unittests and np.unittest module for running tests and getting results
    # Todo: Check if with normalized values stuff needs to be applied differently
    def setUp(self):
        self.p1 = Person(np.array([]), np.array([1, 2, 3, 6]))
        self.p2 = Person(np.array([]), np.array([1, 2, 4, 6]))
        self.p3 = Person(np.array([]), np.array([1, 1, 3, 6]))
        self.p4 = Person(np.array([]), np.array([2, 2, 3, 7]))
        pass

    def test_domnination_for_n_dimensional_kpis(self):
        p1_p2 = utils.dominates(self.p1, self.p2)
        p1_p3 = utils.dominates(self.p1, self.p3)
        p1_p4 = utils.dominates(self.p1, self.p4)
        p2_p3 = utils.dominates(self.p2, self.p3)
        p2_p4 = utils.dominates(self.p2, self.p4)
        p3_p4 = utils.dominates(self.p3, self.p4)

        results = [p1_p2, p1_p3, p1_p4, p2_p3, p2_p4, p3_p4]
        expected = [True, False, True, False, False, True]

        self.assertEqual(expected, results)